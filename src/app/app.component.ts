import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ThemeSelectors } from './layout/state/theme.selectors';
import { LocalStorageService } from './services/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'App';
  themeColor: Observable<any>;

  constructor(
    private _store$: Store
  ) { }

  ngOnInit() {
    this.themeColor = this._store$.pipe(select(ThemeSelectors.getTheme));
  }
}
