import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BitcoinPriceResolver } from './lib/resolvers/bitcoin-price.resolver';
import { ExchangeRateResolver } from './lib/resolvers/exchange-rate.resolver';

const routes: Routes = [

  {
    path: '',
    children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },

    {
      path: 'dashboard',
      //resolve: [BitcoinPriceResolver], // EXCHANGE RATE RESOLVER IS SUPPOSED TO BE HERE BUT CORS ERROR PREVENTS THE HTTP CALL
      loadChildren: () => 
        import('./lib/bitcoin-price/bitcoin-price.module').then(m => m.BitcoinPriceModule),
    }
      
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [
    BitcoinPriceResolver,
    ExchangeRateResolver
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
