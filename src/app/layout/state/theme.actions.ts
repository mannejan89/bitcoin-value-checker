import { createAction, props } from "@ngrx/store";

const getMsg = (name) => `[Theme] ${name}`;

export class ThemeActions {
    static setTheme = createAction(getMsg('Set theme'), props<{ theme: string }>());
}