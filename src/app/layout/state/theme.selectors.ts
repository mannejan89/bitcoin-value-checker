import { createFeatureSelector, createSelector } from "@ngrx/store";
import { LocalStorageService } from "src/app/services/local-storage.service";
import { THEME_FEATURE_KEY } from "./theme.reducer";

export const getState = createFeatureSelector(
    THEME_FEATURE_KEY
);

export class ThemeSelectors {

    static getTheme = createSelector(
        getState,
        state => state
    )

}