import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ThemeSelectors } from './theme.selectors';
import { IThemeState } from './theme.reducer';
import { ThemeActions } from './theme.actions';

@Injectable()
export class ThemeFacade {

    readonly price$ = this._store$.pipe(select(ThemeSelectors.getTheme));

    constructor(
        private _store$: Store<IThemeState>
    ) { }

    setTheme(theme): void {
        this._store$.dispatch(ThemeActions.setTheme({ theme }));
    }

}