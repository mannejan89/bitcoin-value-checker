import { ActionReducer, createReducer, on } from "@ngrx/store";
import { ThemeActions } from './theme.actions';

export const THEME_FEATURE_KEY = 'theme';

export interface IThemeState {
    theme: string
}

export const themeInitialState: IThemeState = {
  theme: JSON.parse(localStorage.getItem('APP-Theme'))?.theme || 'white'
};

const reducer: ActionReducer<IThemeState> = createReducer(
    themeInitialState,

    on(ThemeActions.setTheme, (state, { theme }) => ({ theme })),
)

export function themeReducer(state, action) {
    return reducer(state, action);
}
  