import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { ThemeActions } from "./theme.actions";
import { LocalStorageService } from "src/app/services/local-storage.service";

@Injectable()
export class ThemeEffects {

    setTheme$ = createEffect(() => this._actions$.pipe(
        ofType(ThemeActions.setTheme),
        tap(action => this._localStorageService.setItem('Theme', {
              theme: action.theme
            })
        )
    ), { dispatch: false });

    constructor(
        private _actions$: Actions,
        private _localStorageService: LocalStorageService
    ){}

}