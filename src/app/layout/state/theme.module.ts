import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { themeReducer, THEME_FEATURE_KEY } from './theme.reducer';
import { ThemeFacade } from './theme.facade';
import { EffectsModule } from '@ngrx/effects';
import { ThemeEffects } from './theme.effects';

@NgModule({
  imports: [
    StoreModule.forFeature(THEME_FEATURE_KEY, themeReducer),
    EffectsModule.forFeature([ThemeEffects])
  ],
  providers: [
    ThemeFacade
  ]
})
export class ThemeStateModule {
}
