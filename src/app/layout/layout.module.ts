import { NgModule } from '@angular/core';

import { LayoutShellComponent } from './layout.component';
import { LayoutTopbarComponent } from './topbar/layout-topbar.component';
import { FooterComponent } from './footer/footer.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    LayoutShellComponent,
    LayoutTopbarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,

    MatToolbarModule,
    MatCardModule
  ],
  exports: [
      LayoutTopbarComponent,
      FooterComponent
  ]
})
export class LayoutModule { }
