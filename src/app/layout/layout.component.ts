import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-shell',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutShellComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
