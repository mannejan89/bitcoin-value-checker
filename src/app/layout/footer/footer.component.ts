import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

export interface ICountry {
  name: string,
  flagUrl: string
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  countries: ICountry[] = [
    { name: 'USA', flagUrl: 'https://cdn-icons-png.flaticon.com/512/555/555526.png' },
    { name: 'Canada', flagUrl: 'https://cdn-icons-png.flaticon.com/512/555/555473.png' },
    { name: 'South-Africa', flagUrl: 'https://cdn-icons-png.flaticon.com/512/555/555604.png' },
    { name: 'Germany', flagUrl: 'https://cdn-icons-png.flaticon.com/512/555/555613.png' },
  ];

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  navigateTo(country:string): void {
    this._router.navigateByUrl(`dashboard/${country.toLowerCase()}`);
  }

}
