import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ThemeFacade } from '../state/theme.facade';
import { ThemeSelectors } from '../state/theme.selectors';

@Component({
  selector: 'app-layout-topbar',
  templateUrl: './layout-topbar.component.html', 
  styleUrls: ['./layout-topbar.component.scss']
})
export class LayoutTopbarComponent implements OnInit {

  selectedTheme: Observable<any>;

  constructor(
    private _localStorageService: LocalStorageService,
    private _themeFacade: ThemeFacade,
    private _store$: Store
  ) { }

  ngOnInit(): void {
    this.selectedTheme = this._store$.pipe(select(ThemeSelectors.getTheme));
  }

  changeTheme(theme: string): void {
    this._themeFacade.setTheme(theme);
  }

}
