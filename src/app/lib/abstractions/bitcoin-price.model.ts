import { ITimeResponse } from "./time.model";

export interface IBitcoinResponse {
    bpi: IBitcoinPrice,
    chartName: string,
    disclaimer: string,
    time: ITimeResponse
}

export interface IBitcoinPrice {
    EUR: IBitcoinPriceIndicator,
    USD: IBitcoinPriceIndicator,
    ZAR: IBitcoinPriceIndicator,
    CAD: IBitcoinPriceIndicator
}

export interface IBitcoinPriceIndicator {
    code: string,
    description: string,
    rate: string,
    rate_float: number,
    symbol: string
}