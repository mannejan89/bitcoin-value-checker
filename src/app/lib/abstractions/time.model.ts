export interface ITimeResponse {
    updated: string,
    updatedISO: string,
    updateduk?: string
}