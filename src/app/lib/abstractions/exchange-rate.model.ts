export interface IExchangeRateResponse {
    currency: string,
    rate: Object
}