import { ITimeResponse } from "./time.model";

export interface IBitcoinHistoryResponse {
    bpi: Object,
    disclaimer: string,
    time: ITimeResponse
}

export interface IBitcoinHistory {
    name: string,
    series: {
        name: string,
        value: number
    }[]
}