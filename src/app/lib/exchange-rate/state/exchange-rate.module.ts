import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { EXCHANGE_RATE_FEATURE_KEY, exchangeRateReducer } from './exchange-rate.reducer';
import { ExchangeRateEffects } from './exchange-rate.effects';
import { ExchangeRateFacade } from './exchange-rate.facade';

@NgModule({
  imports: [
    StoreModule.forFeature(EXCHANGE_RATE_FEATURE_KEY, exchangeRateReducer),
    EffectsModule.forFeature([ExchangeRateEffects])
  ],
  providers: [
    ExchangeRateFacade
  ]
})
export class ExchangeRateStateModule {
}
