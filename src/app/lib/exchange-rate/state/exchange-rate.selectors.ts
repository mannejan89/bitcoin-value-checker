import { createFeatureSelector, createSelector, MemoizedSelector } from "@ngrx/store";
import { EXCHANGE_RATE_FEATURE_KEY, IExchangeRateState } from "./exchange-rate.reducer";

export const getState: MemoizedSelector<IExchangeRateState, IExchangeRateState> = createFeatureSelector(
    EXCHANGE_RATE_FEATURE_KEY
);

export class ExchangeRateSelectors {

    static getPrice = createSelector(
        getState,
        state => state.price
    )

    static getLoading = createSelector(
        getState,
        state => state.loading
    );

    static getError = createSelector(
        getState,
        state => state.error
    );

}