import { createAction, props } from "@ngrx/store";

const getMsg = (name) => `[Exchange Rate] ${name}`;

export class ExchangeRateActions {
    static fetch = createAction(getMsg('Fetch'));
    static fetchSuccess = createAction(getMsg('Fetch success'), props<{ rate: any }>());
    static fetchFailure = createAction(getMsg('Fetch failure'), props<{ error: string }>());
}