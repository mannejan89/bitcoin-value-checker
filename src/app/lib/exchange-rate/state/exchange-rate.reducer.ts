import { createEntityAdapter, EntityState } from "@ngrx/entity";
import { ActionReducer, createReducer, on } from "@ngrx/store";
import { IExchangeRateResponse } from "../../abstractions/exchange-rate.model";
import { ExchangeRateActions } from './exchange-rate.actions';

export const EXCHANGE_RATE_FEATURE_KEY = 'exchangeRate';

export interface IExchangeRateState {
    loading: boolean;
    error?: string;
    price: IExchangeRateResponse
  }

export const exchangeRateAdapter = createEntityAdapter<IExchangeRateResponse>();

export const exchangeRateInitialState: IExchangeRateState = {
  loading: false,
  price: {} as IExchangeRateResponse
};

const reducer: ActionReducer<IExchangeRateState> = createReducer(
    exchangeRateInitialState,

    on(ExchangeRateActions.fetch, (state) => ({ ...state, loading: true, error: null })),
    on(ExchangeRateActions.fetchSuccess, (state, { rate }) => ({...state, error: null, loading: false, rate })),

)

export function exchangeRateReducer(state, action) {
    return reducer(state, action);
  }
  