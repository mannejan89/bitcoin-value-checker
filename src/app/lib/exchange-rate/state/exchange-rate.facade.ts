import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ExchangeRateActions } from './exchange-rate.actions';
import { ExchangeRateSelectors } from './exchange-rate.selectors';
import { IExchangeRateState } from './exchange-rate.reducer';

@Injectable()
export class ExchangeRateFacade {

    readonly price$ = this._store$.pipe(select(ExchangeRateSelectors.getPrice));
    readonly loading$ = this._store$.pipe(select(ExchangeRateSelectors.getLoading));
    readonly error$ = this._store$.pipe(select(ExchangeRateSelectors.getError));

    constructor(
        private _store$: Store<IExchangeRateState>
    ) { }

    fetch(): void {
        this._store$.dispatch(ExchangeRateActions.fetch());
    }

}