import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CoinbaseApi } from "../../api/coinbase.api";
import { ExchangeRateActions } from "./exchange-rate.actions";
import { catchError, map, switchMap } from 'rxjs/operators';
import { from, of } from 'rxjs';

@Injectable()
export class ExchangeRateEffects {

    fetch$ = createEffect(() => this._actions$.pipe(
        ofType(ExchangeRateActions.fetch),
        switchMap(() => from(this._coinbaseApi.getCurrentExchangeRate()).pipe(
            map(rate => ExchangeRateActions.fetchSuccess({ rate })),
            catchError(error => of(ExchangeRateActions.fetchFailure({ error })))
        ))
      ));

    constructor(
        private _actions$: Actions,
        private _coinbaseApi: CoinbaseApi,
    ){}

}