import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBitcoinHistoryResponse } from '../abstractions/bitcoin-history.model';
import { IBitcoinResponse } from '../abstractions/bitcoin-price.model';
import { IExchangeRateResponse } from '../abstractions/exchange-rate.model';

@Injectable({providedIn: 'root'})
export class CoinbaseApi {

  private readonly _urlBase = `https://api.coindesk.com/v1`;

  constructor(
    private _httpClient: HttpClient
  ) { }

  async getLatestBitcoinPriceAsync(): Promise<IBitcoinResponse> {
    const url = `${this._urlBase}/bpi/currentprice.json`;
    return this._httpClient.get<IBitcoinResponse>(url).toPromise();
  }

  async getBitcoinPricePerCurrencyAsync(currency: string = 'USD'): Promise<IBitcoinResponse> {
    const url = `${this._urlBase}/bpi/currentprice/${currency}.json`;
    return this._httpClient.get<IBitcoinResponse>(url).toPromise();
  }

  async getBitcoinHistory(startDate, endDate, currency: string = 'USD'): Promise<IBitcoinHistoryResponse> {
    const url = `${this._urlBase}/bpi/historical/close.json?start=${startDate}&end=${endDate}&currency=${currency}`;
    return this._httpClient.get<IBitcoinHistoryResponse>(url).toPromise();
  }

  async getCurrentExchangeRate(currency: string = 'USD'): Promise<IExchangeRateResponse> {
    const url = `${this._urlBase}/exchange-rates`;
    return this._httpClient.get<IExchangeRateResponse>(url, {
      params: { currency }
    }).toPromise();
  }

}
