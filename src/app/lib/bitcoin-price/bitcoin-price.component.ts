import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { CountryCodeService } from 'src/app/services/country-code.service';
import { BitcoinPriceFacade } from './state/bitcoin-price.facade';
import { BitcoinPriceSelectors } from './state/bitcoin-price.selectors';

@Component({
  selector: 'app-bitcoin-price',
  templateUrl: './bitcoin-price.component.html',
  styleUrls: ['./bitcoin-price.component.scss']
})
export class BitcoinPriceComponent implements OnInit {

  readonly loading$ = this._store$.pipe(select(BitcoinPriceSelectors.getLoading));
  readonly bitcoinPrice$ = this._store$.pipe(select(BitcoinPriceSelectors.getPrice));

  bitcoinCountry: string = 'USD';
  countryCurrency: string = 'USD';
  country: string = 'USA';

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _bitcoinPriceFacade: BitcoinPriceFacade,
    private _countryCodeService: CountryCodeService,
    private _store$: Store
  ) {
    this.country = this._activatedRoute.snapshot.routeConfig.path || this.country;
    this.bitcoinCountry = this._countryCodeService.getCountryCode(this.country);
    this.countryCurrency = this._countryCodeService.getCurrencyCode(this.country);

    // FETCH NEW DATA EVERY 2 MINUTES
    setInterval(() => {
      this._bitcoinPriceFacade.fetch(this.bitcoinCountry);
    }, 120000)
  }

  ngOnInit(): void {
  }

}
