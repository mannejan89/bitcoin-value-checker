import { createFeatureSelector, createSelector, MemoizedSelector } from "@ngrx/store";
import { bitcoinPriceAdapter, BITCOIN_PRICE_FEATURE_KEY, IBitcoinPriceState } from "./bitcoin-price.reducer";

export const getState: MemoizedSelector<IBitcoinPriceState, IBitcoinPriceState> = createFeatureSelector(
    BITCOIN_PRICE_FEATURE_KEY
);

export class BitcoinPriceSelectors {

    static getPrice = createSelector(
        getState,
        state => state.price
    )

    static getLoading = createSelector(
        getState,
        state => state.loading
    );

    static getError = createSelector(
        getState,
        state => state.error
    );

}