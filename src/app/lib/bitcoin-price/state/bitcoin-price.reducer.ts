import { createEntityAdapter, EntityState } from "@ngrx/entity";
import { ActionReducer, createReducer, on } from "@ngrx/store";
import { IBitcoinPrice } from "../../abstractions/bitcoin-price.model";
import { BitcoinPriceActions } from './bitcoin-price.actions';

export const BITCOIN_PRICE_FEATURE_KEY = 'bitcoinPrice';

export interface IBitcoinPriceState {
    loading: boolean;
    error?: string;
    price: IBitcoinPrice
  }

export const bitcoinPriceAdapter = createEntityAdapter<IBitcoinPrice>();

export const bitcoinPriceInitialState: IBitcoinPriceState = {
  loading: false,
  price: {} as IBitcoinPrice
};

const reducer: ActionReducer<IBitcoinPriceState> = createReducer(
    bitcoinPriceInitialState,

    on(BitcoinPriceActions.fetch, (state) => ({ ...state, loading: true, error: null })),
    on(BitcoinPriceActions.fetchSuccess, (state, { price }) => ({...state, error: null, loading: false, price })),

)

export function bitcoinPriceReducer(state, action) {
    return reducer(state, action);
  }
  