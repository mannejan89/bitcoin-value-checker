import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BitcoinPriceActions } from './bitcoin-price.actions';
import { BitcoinPriceSelectors } from './bitcoin-price.selectors';
import { IBitcoinPriceState } from './bitcoin-price.reducer';

@Injectable()
export class BitcoinPriceFacade {

    readonly price$ = this._store$.pipe(select(BitcoinPriceSelectors.getPrice));
    readonly loading$ = this._store$.pipe(select(BitcoinPriceSelectors.getLoading));
    readonly error$ = this._store$.pipe(select(BitcoinPriceSelectors.getError));

    constructor(
        private _store$: Store<IBitcoinPriceState>
    ) { }

    fetch(currency:string = 'USD'): void {
        this._store$.dispatch(BitcoinPriceActions.fetch({ currency }));
    }

}