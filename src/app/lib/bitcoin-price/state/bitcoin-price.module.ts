import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BITCOIN_PRICE_FEATURE_KEY, bitcoinPriceReducer } from './bitcoin-price.reducer';
import { BitcoinPriceEffects } from './bitcoin-price.effects';
import { BitcoinPriceFacade } from './bitcoin-price.facade';

@NgModule({
  imports: [
    StoreModule.forFeature(BITCOIN_PRICE_FEATURE_KEY, bitcoinPriceReducer),
    EffectsModule.forFeature([BitcoinPriceEffects])
  ],
  providers: [
    BitcoinPriceFacade
  ]
})
export class BitcoinPriceStateModule {
}
