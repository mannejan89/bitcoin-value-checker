import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CoinbaseApi } from "../../api/coinbase.api";
import { BitcoinPriceActions } from "./bitcoin-price.actions";
import { catchError, map, switchMap } from 'rxjs/operators';
import { from, of } from 'rxjs';

@Injectable()
export class BitcoinPriceEffects {

    fetch$ = createEffect(() => this._actions$.pipe(
        ofType(BitcoinPriceActions.fetch),
        switchMap((action) => from(this._coinbaseApi.getBitcoinPricePerCurrencyAsync(action.currency)).pipe(
            map(price => BitcoinPriceActions.fetchSuccess({ price: price.bpi })),
            catchError(error => of(BitcoinPriceActions.fetchFailure({ error })))
        ))
      ));

    constructor(
        private _actions$: Actions,
        private _coinbaseApi: CoinbaseApi,
    ){}

}