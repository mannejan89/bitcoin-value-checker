import { createAction, props } from "@ngrx/store";
import { IBitcoinPrice } from "../../abstractions/bitcoin-price.model";

const getMsg = (name) => `[Bitcoin Price] ${name}`;

export class BitcoinPriceActions {
    static fetch = createAction(getMsg('Fetch'), props<{ currency: string }>());
    static fetchSuccess = createAction(getMsg('Fetch success'), props<{ price: IBitcoinPrice }>());
    static fetchFailure = createAction(getMsg('Fetch failure'), props<{ error: string }>());
}