import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BitcoinPriceComponent } from './bitcoin-price.component';
import { BitcoinPriceStateModule } from './state/bitcoin-price.module';
import { RouterModule, Routes } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';

import { RefreshPriceComponent } from './components/refresh-price/refresh-price.component';
import { ThemeStateModule } from 'src/app/layout/state/theme.module';
import { BitcoinConverterComponent } from './components/bitcoin-converter/bitcoin-converter.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { ConvertBitcoinToCurrencyPipe } from './components/bitcoin-converter/pipes/convert-bitcoin-to-currency.pipe';
import { ConvertCurrencyToBitcoinPipe } from './components/bitcoin-converter/pipes/convert-currency-to-bitcoin.pipe';
import { ExchangeRateStateModule } from '../exchange-rate/state/exchange-rate.module';
import { BitcoinPriceResolver } from '../resolvers/bitcoin-price.resolver';
import { BitcoinHistoryComponent } from './components/bitcoin-history/bitcoin-history.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BitcoinHistoryStateModule } from './components/bitcoin-history/state/bitcoin-history.module';

const routes: Routes = [

  {
    path: '',
    component: BitcoinPriceComponent,
    resolve: [BitcoinPriceResolver]
  },
  {
    path: 'south-africa',
    component: BitcoinPriceComponent,
    resolve: [BitcoinPriceResolver]
  },
  {
    path: 'canada',
    component: BitcoinPriceComponent,
    resolve: [BitcoinPriceResolver]
  },
  {
    path: 'germany',
    component: BitcoinPriceComponent,
    resolve: [BitcoinPriceResolver]
  },
  { 
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },

];

@NgModule({
  declarations: [
    BitcoinPriceComponent,
    RefreshPriceComponent,
    BitcoinConverterComponent,
    ConvertBitcoinToCurrencyPipe,
    ConvertCurrencyToBitcoinPipe,
    BitcoinHistoryComponent
  ],
  imports: [
    CommonModule,
    BitcoinPriceStateModule,
    ExchangeRateStateModule,
    ThemeStateModule,
    BitcoinHistoryStateModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    
    MatButtonModule,
    MatInputModule,
    MatProgressSpinnerModule,
    NgxChartsModule
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'outline',
        floatLabel: 'never'
      }
    }
  ]
})
export class BitcoinPriceModule { }
