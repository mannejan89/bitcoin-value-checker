import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { BitcoinPriceFacade } from '../../state/bitcoin-price.facade';

@Component({
  selector: 'app-bitcoin-converter',
  templateUrl: './bitcoin-converter.component.html',
  styleUrls: ['./bitcoin-converter.component.scss']
})
export class BitcoinConverterComponent implements OnInit {

  @Input() public currencyCode: string = 'USD';

  bitcoinToCurrencyFormControl = new FormControl('');
  currencyToBitcoinFormControl = new FormControl('')
  
  bitcoinFormGroup: FormGroup = new FormGroup({
    bitcoinToCurrency: this.bitcoinToCurrencyFormControl,
    currencyToBitcoin: this.currencyToBitcoinFormControl
  });

  constructor(
    private _localStorageService: LocalStorageService
  ) {
    this.bitcoinToCurrencyFormControl.setValue(this._localStorageService.getItem('totalBitcoin'))
  }

  ngOnInit(): void {
  }

  saveValueToStorage(key:string, value: number): void {
    this._localStorageService.setItem(key, value);
  }

}
