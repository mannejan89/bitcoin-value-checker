import { formatCurrency, getCurrencySymbol } from '@angular/common';
import { OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IBitcoinPrice } from 'src/app/lib/abstractions/bitcoin-price.model';
import { BitcoinPriceSelectors } from '../../../state/bitcoin-price.selectors';

@Pipe({
  name: 'convertBitcoinToCurrency'
})
export class ConvertBitcoinToCurrencyPipe implements PipeTransform, OnDestroy {

  destroy$ = new Subject();
  private _bitcoinPrice: IBitcoinPrice;

  constructor(
    private _store$: Store
  ){
    this._store$.pipe(
      select(BitcoinPriceSelectors.getPrice),
      takeUntil(this.destroy$)
    ).subscribe(value => {
      this._bitcoinPrice = value;
    })
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  transform(value: number, ...args: unknown[]): unknown {
    if(!value || typeof value !== 'number') return;
    const currency = args[0].toString() || 'USD';

    const bitcoinValue = value * this._bitcoinPrice[currency].rate_float;
    const currencySymbol = getCurrencySymbol(currency, 'narrow');

    return formatCurrency(bitcoinValue, 'en', currencySymbol);
  }

}
