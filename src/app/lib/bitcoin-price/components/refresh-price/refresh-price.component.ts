import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BitcoinPriceFacade } from '../../state/bitcoin-price.facade';
import { BitcoinPriceSelectors } from '../../state/bitcoin-price.selectors';

@Component({
  selector: 'app-refresh-price',
  templateUrl: './refresh-price.component.html',
  styleUrls: ['./refresh-price.component.scss']
})
export class RefreshPriceComponent implements OnInit {

  @Input() private currencyCode: string;

  disableRefresh = false;

  constructor(
    private _bitcoinPriceFacade: BitcoinPriceFacade,
    private _store$: Store
  ) { }

  ngOnInit(): void {
  }

  refreshPrice(){
    this._bitcoinPriceFacade.fetch(this.currencyCode);
    this.disableRefreshButton();
  }

  private disableRefreshButton(): void {
    this.disableRefresh = true;
    setTimeout(() => {
      this.disableRefresh = false;
    }, 5000)
  }

}
