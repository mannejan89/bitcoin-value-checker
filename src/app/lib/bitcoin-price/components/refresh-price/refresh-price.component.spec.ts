import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefreshPriceComponent } from './refresh-price.component';

describe('RefreshPriceComponent', () => {
  let component: RefreshPriceComponent;
  let fixture: ComponentFixture<RefreshPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefreshPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefreshPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
