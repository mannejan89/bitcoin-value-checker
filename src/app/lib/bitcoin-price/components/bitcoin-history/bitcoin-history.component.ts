import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { last, takeUntil } from 'rxjs/operators';
import { IBitcoinHistory } from 'src/app/lib/abstractions/bitcoin-history.model';
import { BitcoinHistoryFacade } from './state/bitcoin-history.facade';
import { BitcoinHistorySelectors } from './state/bitcoin-history.selectors';
import * as moment from 'moment';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-bitcoin-history',
  templateUrl: './bitcoin-history.component.html',
  styleUrls: ['./bitcoin-history.component.scss']
})
export class BitcoinHistoryComponent implements OnInit, OnChanges, OnDestroy {

  destroy$ = new Subject();

  @Input() private currencyCode: string;
  @Input() public countryCurrency: string;

  readonly history$ = this._store$.pipe(select(BitcoinHistorySelectors.getHistory));
  readonly loading$ = this._store$.pipe(select(BitcoinHistorySelectors.getLoading));

  historyData

  colorScheme = {
    domain: ['#E44D25']
  };

  selectedDatePeriod:string;

  constructor(
    private _bitcoinHistoryFacade: BitcoinHistoryFacade,
    private _store$: Store,
    private _localStorageService: LocalStorageService
  ) {
    this.history$
    .pipe(takeUntil(this.destroy$))
    .subscribe(response => this.historyData = response)
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void{
    let historyAmount = Number(this._localStorageService.getItem('history')?.amount) || 7;
    let historyUnit = this._localStorageService.getItem('history')?.unit.toString() || 'days';
    
    if(changes?.currencyCode)
      this.changeBitcoinHistory(historyAmount, historyUnit);
  }

  changeBitcoinHistory(amount: moment.DurationInputArg1, unit: moment.unitOfTime.DurationConstructor): void {
    this.selectedDatePeriod = `${amount.toString()}${unit.charAt(0).toUpperCase()}`;

    let startDate = moment().subtract(amount, unit).toDate();
    let endDate = moment().subtract(1, "days").toDate(); // GET YESTARDAY
    this._bitcoinHistoryFacade.fetch(startDate, endDate, this.currencyCode);

    this._localStorageService.setItem('history', { amount, unit });
  }

}
