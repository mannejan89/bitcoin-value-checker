import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { CoinbaseApi } from "src/app/lib/api/coinbase.api";
import { BitcoinHistoryActions } from "./bitcoin-history.actions";

@Injectable()
export class BitcoinHistoryEffects {

    fetch$ = createEffect(() => this._actions$.pipe(
        ofType(BitcoinHistoryActions.fetch),
        switchMap((action) => from(this._coinbaseApi.getBitcoinHistory(action.startDate, action.endDate, action.currency)).pipe(
            map(history => BitcoinHistoryActions.fetchSuccess({ history: [{
                name: 'history',
                series: Object.entries(history.bpi).map(([name, value]) => ({ name, value })) 
            }]})),
            catchError(error => of(BitcoinHistoryActions.fetchFailure({ error })))
        ))
    ));

    constructor(
        private _actions$: Actions,
        private _coinbaseApi: CoinbaseApi
    ){}

}