import { createFeatureSelector, createSelector, MemoizedSelector } from "@ngrx/store";
import { bitcoinHistoryAdapter, BITCOIN_HISTORY_FEATURE_KEY, IBitcoinHistoryState } from "./bitcoin-history.reducer";

export const getState: MemoizedSelector<IBitcoinHistoryState, IBitcoinHistoryState> = createFeatureSelector(
    BITCOIN_HISTORY_FEATURE_KEY
);

export class BitcoinHistorySelectors {

    static getHistory = createSelector(
        getState,
        state => state.history
    )

    static getLoading = createSelector(
        getState,
        state => state.loading
    );

    static getError = createSelector(
        getState,
        state => state.error
    );

}