import { createAction, props } from "@ngrx/store";
import { IBitcoinHistory, IBitcoinHistoryResponse } from "src/app/lib/abstractions/bitcoin-history.model";

const getMsg = (name) => `[Bitcoin History] ${name}`;

export class BitcoinHistoryActions {
    static fetch = createAction(getMsg('Fetch'), props< { startDate: string, endDate: string, currency: string }>());
    static fetchSuccess = createAction(getMsg('Fetch success'), props<{ history: IBitcoinHistory[] }>());
    static fetchFailure = createAction(getMsg('Fetch failure'), props<{ error: string }>());
}