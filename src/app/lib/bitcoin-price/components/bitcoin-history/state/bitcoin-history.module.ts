import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { bitcoinHistoryReducer, BITCOIN_HISTORY_FEATURE_KEY } from './bitcoin-history.reducer';
import { BitcoinHistoryEffects } from './bitcoin-history.effects';
import { BitcoinHistoryFacade } from './bitcoin-history.facade';

@NgModule({
  imports: [
    StoreModule.forFeature(BITCOIN_HISTORY_FEATURE_KEY, bitcoinHistoryReducer),
    EffectsModule.forFeature([BitcoinHistoryEffects])
  ],
  providers: [
    BitcoinHistoryFacade
  ]
})
export class BitcoinHistoryStateModule {
}
