import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BitcoinHistoryActions } from './bitcoin-history.actions';
import { IBitcoinHistoryState } from './bitcoin-history.reducer';
import { BitcoinHistorySelectors } from './bitcoin-history.selectors';

@Injectable()
export class BitcoinHistoryFacade {

    readonly price$ = this._store$.pipe(select(BitcoinHistorySelectors.getHistory));
    readonly loading$ = this._store$.pipe(select(BitcoinHistorySelectors.getLoading));
    readonly error$ = this._store$.pipe(select(BitcoinHistorySelectors.getError));

    constructor(
        private _store$: Store<IBitcoinHistoryState>
    ) { }

    fetch(start: Date, end: Date, currency: string): void {
        let startDate = formatDate(start, 'yyyy-MM-dd', 'en');
        let endDate = formatDate(end, 'yyyy-MM-dd', 'en');
        this._store$.dispatch(BitcoinHistoryActions.fetch({ startDate, endDate, currency }));
    }

}