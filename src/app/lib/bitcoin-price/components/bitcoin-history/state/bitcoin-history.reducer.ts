import { createEntityAdapter } from "@ngrx/entity";
import { ActionReducer, createReducer, on } from "@ngrx/store";
import { IBitcoinHistory } from "src/app/lib/abstractions/bitcoin-history.model";
import { BitcoinHistoryActions } from "./bitcoin-history.actions";

export const BITCOIN_HISTORY_FEATURE_KEY = 'bitcoinHistory';

export interface IBitcoinHistoryState {
    loading: boolean;
    error?: string;
    history: IBitcoinHistory[]
  }

export const bitcoinHistoryAdapter = createEntityAdapter<IBitcoinHistory>();

export const bitcoinHistoryInitialState: IBitcoinHistoryState = {
  loading: false,
  history: []
};

const reducer: ActionReducer<IBitcoinHistoryState> = createReducer(
    bitcoinHistoryInitialState,

    on(BitcoinHistoryActions.fetch, (state) => ({ ...state, loading: true, error: null })),
    on(BitcoinHistoryActions.fetchSuccess, (state, { history }) => ({...state, error: null, loading: false, history })),

)

export function bitcoinHistoryReducer(state, action) {
    return reducer(state, action);
  }
  