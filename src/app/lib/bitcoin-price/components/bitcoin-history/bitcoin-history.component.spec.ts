import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitcoinHistoryComponent } from './bitcoin-history.component';

describe('BitcoinHistoryComponent', () => {
  let component: BitcoinHistoryComponent;
  let fixture: ComponentFixture<BitcoinHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitcoinHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitcoinHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
