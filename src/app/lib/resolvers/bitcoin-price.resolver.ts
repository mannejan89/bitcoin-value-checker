import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Actions, ofType } from '@ngrx/effects';
import { first } from 'rxjs/operators';
import { CountryCodeService } from 'src/app/services/country-code.service';
import { IBitcoinPrice } from "../abstractions/bitcoin-price.model";
import { BitcoinPriceActions } from '../bitcoin-price/state/bitcoin-price.actions';
import { BitcoinPriceFacade } from "../bitcoin-price/state/bitcoin-price.facade";

@Injectable()
export class BitcoinPriceResolver implements Resolve<Promise<IBitcoinPrice>> {
  constructor(
    private _actions: Actions,
    private _bitcoinPriceFacade: BitcoinPriceFacade,
    private _countryCodeService: CountryCodeService
  ) { }

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){

      let country = route.routeConfig.path || 'USA';
      let bitcoinCurrency = this._countryCodeService.getCountryCode(country);

      const resolver = new Promise<IBitcoinPrice>(async (resolve, reject) => {
        const error = await Promise.race([
          this._actions.pipe(ofType(BitcoinPriceActions.fetchSuccess), first()).toPromise().then(() => false),
          this._actions.pipe(ofType(BitcoinPriceActions.fetchFailure), first()).toPromise().then(() => true)
        ]);
  
        if (error) return reject();
  
        const bitcoinPrice = await this._bitcoinPriceFacade.price$.pipe(first()).toPromise();
        resolve(bitcoinPrice);
      });
  
      this._bitcoinPriceFacade.fetch(bitcoinCurrency);
  
      return resolver;
    }
}