import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Actions, ofType } from '@ngrx/effects';
import { first } from 'rxjs/operators';
import { IExchangeRateResponse } from "../abstractions/exchange-rate.model";
import { ExchangeRateActions } from '../exchange-rate/state/exchange-rate.actions';
import { ExchangeRateFacade } from "../exchange-rate/state/exchange-rate.facade";

@Injectable()
export class ExchangeRateResolver implements Resolve<Promise<IExchangeRateResponse>> {
  constructor(
    private _actions: Actions,
    private _exchangeRateFacade: ExchangeRateFacade
  ) { }

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
      const resolver = new Promise<IExchangeRateResponse>(async (resolve, reject) => {
        const error = await Promise.race([
          this._actions.pipe(ofType(ExchangeRateActions.fetchSuccess), first()).toPromise().then(() => false),
          this._actions.pipe(ofType(ExchangeRateActions.fetchFailure), first()).toPromise().then(() => true)
        ]);
  
        if (error) return reject();
  
        const exchangeRate = await this._exchangeRateFacade.price$.pipe(first()).toPromise();
        resolve(exchangeRate);
      });
  
      this._exchangeRateFacade.fetch();
  
      return resolver;
    }
}