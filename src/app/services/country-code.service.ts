import { Injectable } from '@angular/core';

export interface Country {
  name: string;
  code: string;
  currency?: string,
}

@Injectable({
  providedIn: 'root'
})
export class CountryCodeService {

  constructor() { }

  countryMap: Country[] = [
    { name: 'usa', code: 'USD' },
    { name: 'canada', code: 'CAD'},
    { name: 'germany', code: 'EUR' },
    { name: 'south-africa', currency: 'R', code: 'ZAR'}
  ]

  getCurrencyCode(countryName: string): string {
    const country: Country | undefined = this.countryMap.find((c: Country) => c.name === countryName);
    return country?.currency || country?.code || 'USD';
  }

  getCountryCode(countryName: string): string {
    const country: Country | undefined = this.countryMap.find((c: Country) => c.name === countryName);
    return country?.code || 'USD';
  }

}
